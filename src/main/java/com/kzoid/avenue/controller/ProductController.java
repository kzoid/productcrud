package com.kzoid.avenue.controller;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kzoid.avenue.entity.ImageEntity;
import com.kzoid.avenue.entity.ProductEntity;
import com.kzoid.avenue.service.ImageService;
import com.kzoid.avenue.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 20/01/2017.
 */
@RestController
@JsonSerialize
public class ProductController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ImageService imageService;

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public ResponseEntity<List<ProductEntity>> getProducts(@RequestParam(value = "include", required = false) String include) throws Exception {
        List<ProductEntity> products;

        if (include != null) include.toLowerCase(); else include = "";
        switch (include) {
            case "all":
                products = productService.findAllProductsWithSubproductsImages();
                break;
            case "subproducts":
                products = productService.findAllProductsWithSubproducts();
                break;
            case "images":
                products = productService.findAllProductsWithImages();
                break;
            default:
                products = productService.findAllProducts();
        }

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProductEntity> getProduct(@PathVariable(value = "id") int id, @RequestParam(value = "include", required = false) String include) throws Exception {
        ProductEntity product;

        if (include != null) include.toLowerCase(); else include = "";
        switch (include) {
            case "all":
                product = productService.findByIdWithSubproductsImages(id);
                break;
            case "subproducts":
                product = productService.findByIdWithSubproducts(id);
                break;
            case "images":
                product = productService.findByIdWithImages(id);
                break;
            default:
                product = productService.findById(id);
        }

        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @RequestMapping(value = "/product/{id}/subproducts", method = RequestMethod.GET)
    public ResponseEntity<List<ProductEntity>> getProductsChilds(@PathVariable(value = "id") int id) throws Exception {

        return new ResponseEntity<>(productService.findAllChildProducts(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/product/{id}/images", method = RequestMethod.GET)
    public ResponseEntity<List<ImageEntity>> getProductsImages(@PathVariable(value = "id") int id) throws Exception {

        return new ResponseEntity<>(imageService.findImagesByProduct(id), HttpStatus.OK);
    }

}