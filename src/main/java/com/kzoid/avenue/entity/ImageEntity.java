package com.kzoid.avenue.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Ricardo Gelschleiter on 20/01/2017.
 */
@Entity
@Table(name = "images")
public class ImageEntity implements Serializable {

    /**
     * The ID of the image.
     */
    @Id
    @Column(name = "image_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * The ID of the product.
     */
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = ProductEntity.class)
    @JoinColumn(name = "product_id")
    @JsonBackReference
    private ProductEntity product;

    /**
     * The uri for the image source.
     */
    @Column(name = "uri", nullable = false, length = 200)
    private String uri;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}