package com.kzoid.avenue.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 20/01/2017.
 */
@Entity
@Table(name = "products")
public class ProductEntity implements Serializable {

    /**
     * The unique ID of the product.
     */
    @Id
    @Column(name = "product_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * The parent product (optional).
     */
    @ManyToOne(targetEntity = ProductEntity.class)
    @JoinColumn(name = "parent_id")
    @JsonBackReference
    private ProductEntity parent;

    /**
     * The list of subproducts related to this product (optional).
     */
    @OneToMany(fetch = FetchType.LAZY, targetEntity = ProductEntity.class, mappedBy = "parent")
    @JsonManagedReference
    public List subProducts;

    /**
     * The list of images related to this product (optional).
     */
    @OneToMany(fetch = FetchType.LAZY, targetEntity = ImageEntity.class, mappedBy = "product")
    @JsonManagedReference
    private List images;

    /**
     * The name of the product.
     */
    @Column(name = "name", nullable = false, length = 100)
    private String name;

    public ProductEntity() {
    }

    public ProductEntity(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductEntity getParent() {
        return parent;
    }

    public void setParent(ProductEntity parent) {
        this.parent = parent;
    }

    public List getSubProducts() {
        return subProducts;
    }

    public void setSubProducts(List subProducts) {
        this.subProducts = subProducts;
    }

    public List getImages() {
        return images;
    }

    public void setImages(List images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}