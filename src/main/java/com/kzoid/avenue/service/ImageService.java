package com.kzoid.avenue.service;

import com.kzoid.avenue.entity.ImageEntity;

import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 22/01/2017.
 */
public interface ImageService {

    List<ImageEntity> findImagesByProduct(int productId);
}
