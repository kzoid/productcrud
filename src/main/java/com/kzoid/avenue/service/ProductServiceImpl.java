package com.kzoid.avenue.service;

import com.kzoid.avenue.dao.ProductDAO;
import com.kzoid.avenue.entity.ProductEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 22/01/2017.
 */
@Component
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDAO productDAO;

    @Override
    public ProductEntity findById(int id) {
        return productDAO.findById(id);
    }

    @Override
    public List<ProductEntity> findAllProducts() {
        return productDAO.findAllProducts();
    }

    @Override
    public List<ProductEntity> findAllProductsWithSubproducts() {
        return productDAO.findAllProductsWithSubproducts();
    }

    @Override
    public ProductEntity findByIdWithSubproducts(int id) {
        return productDAO.findByIdWithSubproducts(id);
    }

    @Override
    public List<ProductEntity> findAllProductsWithImages() {
        return productDAO.findAllProductsWithImages();
    }

    @Override
    public ProductEntity findByIdWithImages(int id) {
        return productDAO.findByIdWithImages(id);
    }

    @Override
    public List<ProductEntity> findAllProductsWithSubproductsImages() {
        return productDAO.findAllProductsWithSubproductsImages();
    }

    @Override
    public ProductEntity findByIdWithSubproductsImages(int id) {
        return productDAO.findByIdWithSubproductsImages(id);
    }

    @Override
    public List<ProductEntity> findAllChildProducts(int productId) {
        return productDAO.findAllChildProducts(productId);
    }
}