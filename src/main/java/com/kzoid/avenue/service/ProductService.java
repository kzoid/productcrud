package com.kzoid.avenue.service;

import com.kzoid.avenue.entity.ProductEntity;

import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 22/01/2017.
 */
public interface ProductService {

    ProductEntity findById(int id);

    List<ProductEntity> findAllProducts();

    List<ProductEntity> findAllProductsWithSubproducts();

    ProductEntity findByIdWithSubproducts(int id);

    List<ProductEntity> findAllProductsWithImages();

    ProductEntity findByIdWithImages(int id);

    List<ProductEntity> findAllProductsWithSubproductsImages();

    ProductEntity findByIdWithSubproductsImages(int id);

    List<ProductEntity> findAllChildProducts(int productId);
}
