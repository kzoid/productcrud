package com.kzoid.avenue.service;

import com.kzoid.avenue.dao.ImageDAO;
import com.kzoid.avenue.entity.ImageEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 22/01/2017.
 */
@Component
public class ImageServiceImpl implements ImageService {

    @Autowired
    ImageDAO imageDAO;

    @Override
    public List<ImageEntity> findImagesByProduct(int productId) {
        return imageDAO.findImagesByProduct(productId);
    }
}
