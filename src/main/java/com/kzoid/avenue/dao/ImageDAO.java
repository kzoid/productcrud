package com.kzoid.avenue.dao;

import com.kzoid.avenue.entity.ImageEntity;

import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 22/01/2017.
 */
public interface ImageDAO {

    /**
     * f) Get set of images for specific product
     *
     * @param product_id related product
     * @return a list of images of the product
     */
    List<ImageEntity> findImagesByProduct(int product_id);
}
