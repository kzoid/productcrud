package com.kzoid.avenue.dao;

import com.kzoid.avenue.entity.ProductEntity;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 22/01/2017.
 */
@Repository
@Transactional
public class ProductDAOImpl extends AbstractDao<Integer, ProductEntity> implements ProductDAO {

    @Override
    public List<ProductEntity> findAllProducts() {
        return (List<ProductEntity>) getSession().createCriteria(ProductEntity.class).list();
    }

    @Override
    public ProductEntity findById(int id) {
        return this.getByKey(id);
    }

    @Override
    public List<ProductEntity> findAllProductsWithSubproducts() {
        List<ProductEntity> products = (List<ProductEntity>) getSession().createCriteria(ProductEntity.class).list();

        if (products != null)
            products.forEach(productEntity -> Hibernate.initialize(productEntity.getSubProducts()));

        return products;
    }

    @Override
    public ProductEntity findByIdWithSubproducts(int id) {
        ProductEntity product = this.findById(id);

        if (product != null)
            Hibernate.initialize(product.getSubProducts());

        return product;
    }

    @Override
    public List<ProductEntity> findAllProductsWithImages() {
        List<ProductEntity> products = (List<ProductEntity>) getSession().createCriteria(ProductEntity.class).list();

        if (products != null)
            products.forEach(productEntity -> Hibernate.initialize(productEntity.getImages()));

        return products;
    }

    @Override
    public ProductEntity findByIdWithImages(int id) {
        ProductEntity product = this.findById(id);
        if (product != null)
            Hibernate.initialize(product.getImages());

        return product;
    }

    @Override
    public List<ProductEntity> findAllProductsWithSubproductsImages() {
        List<ProductEntity> products = (List<ProductEntity>) getSession().createCriteria(ProductEntity.class).list();

        if (products != null) {
            products.forEach(productEntity -> Hibernate.initialize(productEntity.getImages()));
            products.forEach(productEntity -> Hibernate.initialize(productEntity.getSubProducts()));
        }

        return products;
    }

    @Override
    public ProductEntity findByIdWithSubproductsImages(int id) {
        ProductEntity product = this.findById(id);
        if (product != null) {
            Hibernate.initialize(product.getSubProducts());
            Hibernate.initialize(product.getImages());
        }

        return product;
    }

    @Override
    public List<ProductEntity> findAllChildProducts(int productId) {
        return (List<ProductEntity>) getSession().createCriteria(ProductEntity.class).add(Restrictions.eq("parent", new ProductEntity(productId))).list();
    }
}
