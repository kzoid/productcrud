package com.kzoid.avenue.dao;

import com.kzoid.avenue.entity.ProductEntity;

import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 22/01/2017.
 */
public interface ProductDAO {

    /**
     * a) Get all products excluding relationships (child products, images)
     *
     * @return a list of products in base
     */
    List<ProductEntity> findAllProducts();

    /**
     * c) Same as 1 using specific product identity
     *
     * @param productId product identity
     * @return
     */
    ProductEntity findById(int productId);

    /**
     * b) Get all products including specified relationships (child product and/or images)
     *
     * @return a list of products including sub product relationships
     */
    List<ProductEntity> findAllProductsWithSubproducts();

    /**
     * d) Same as 2 using specific product identity
     *
     * @return a list of products including sub product relationships of a specific product
     */
    ProductEntity findByIdWithSubproducts(int productId);

    /**
     * b) Get all products including specified relationships (child product and/or images)
     *
     * @return a list of products including images relationships
     */
    List<ProductEntity> findAllProductsWithImages();

    /**
     * d) Same as 2 using specific product identity
     *
     * @return a list of products including images relationships of a specific product
     */
    ProductEntity findByIdWithImages(int productId);

    /**
     * b) Get all products including specified relationships (child product and/or images)
     *
     * @return a list of products including sub product and images relationships
     */
    List<ProductEntity> findAllProductsWithSubproductsImages();

    /**
     * d) Same as 2 using specific product identity
     *
     * @return a list of products including sub product and images relationships of a specific product
     */
    ProductEntity findByIdWithSubproductsImages(int productId);

    /**
     * e) Get set of child products for specific product
     *
     * @param parentId Parent Product
     * @return a list with related products
     */
    List<ProductEntity> findAllChildProducts(int parentId);
}