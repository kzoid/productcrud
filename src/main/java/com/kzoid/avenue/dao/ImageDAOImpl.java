package com.kzoid.avenue.dao;

import com.kzoid.avenue.entity.ImageEntity;
import com.kzoid.avenue.entity.ProductEntity;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 22/01/2017.
 */
@Repository
@Transactional
public class ImageDAOImpl extends AbstractDao<Integer, ImageEntity> implements ImageDAO {

    @Override
    public List<ImageEntity> findImagesByProduct(int product_id) {
        return (List<ImageEntity>) getSession().createCriteria(ImageEntity.class).add(Restrictions.eq("product", new ProductEntity(product_id))).list();
    }
}
