INSERT INTO products VALUES (1, NULL, 'Fruit Ninja');
INSERT INTO products VALUES (2, 1, 'Coffee & Spice Grinder');

INSERT INTO products VALUES (3, NULL, 'KitchenAid');
INSERT INTO products VALUES (4, 3, 'KitchenAid Masticating Juicer and Sauce Attachment');
INSERT INTO products VALUES (5, 3, 'KitchenAid Food Grinder Attachment');

INSERT INTO products VALUES (6, NULL, 'Mr. Coffee');

INSERT INTO images VALUES (1, 1, 'https://images-na.ssl-images-amazon.com/images/I/81MgVRG6DdL._SL1500_.jpg');
INSERT INTO images VALUES (2, 2, 'https://images-na.ssl-images-amazon.com/images/I/41TQrs24TjL.jpg');
INSERT INTO images VALUES (3, 3, 'https://images-na.ssl-images-amazon.com/images/I/61lSnfvQONL._SL1000_.jpg');
INSERT INTO images VALUES (4, 3, 'https://images-na.ssl-images-amazon.com/images/I/61cjbywzhtL._SL1000_.jpg');
INSERT INTO images VALUES (5, 4, 'https://images-na.ssl-images-amazon.com/images/I/61Pc4DSRLjL._SL1000_.jpg');
INSERT INTO images VALUES (6, 5, 'https://images-na.ssl-images-amazon.com/images/I/41FZdPPhWDL.jpg');
INSERT INTO images VALUES (7, 6, 'https://images-na.ssl-images-amazon.com/images/I/91CRKmaFW6L._SL1500_.jpg');