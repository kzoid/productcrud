DROP TABLE images IF EXISTS;
DROP TABLE products IF EXISTS;

CREATE TABLE products (
  product_id  INTEGER PRIMARY KEY,
  parent_id   INTEGER,
  name        VARCHAR(100)
);

CREATE TABLE images (
  image_id    INTEGER PRIMARY KEY,
  product_id  INT,
  uri         VARCHAR(200),
  FOREIGN KEY (product_id)
  REFERENCES products (product_id)
    ON DELETE CASCADE
)