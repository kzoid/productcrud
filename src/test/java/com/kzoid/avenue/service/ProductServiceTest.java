package com.kzoid.avenue.service;

import com.kzoid.avenue.config.AppConfig;
import com.kzoid.avenue.dao.ImageDAO;
import com.kzoid.avenue.dao.ProductDAO;
import com.kzoid.avenue.entity.ImageEntity;
import com.kzoid.avenue.entity.ProductEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 21/01/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppConfig.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProductServiceTest {

    @Autowired
    ImageDAO imageDAO;

    @Autowired
    ProductDAO productDAO;

    @Test
    public void testImageCount() {
        List<ImageEntity> images = imageDAO.findImagesByProduct(3);

        //expect 2 images
        Assert.assertNotNull(images);
        Assert.assertTrue(images.size() > 1);
    }

    @Test
    public void testFindById() {
        ProductEntity product = productDAO.findById(6);

        Assert.assertNotNull(product);
        Assert.assertEquals(6, product.getId());
        Assert.assertEquals("Mr. Coffee", product.getName());
    }

    @Test
    public void testFindProductWithImages() {
        ProductEntity product = productDAO.findByIdWithImages(6);

        Assert.assertNotNull(product);
        Assert.assertNotNull(product.getImages());
    }

    @Test(expected = org.hibernate.LazyInitializationException.class)
    public void testFindProductWithoutImagesLazyError() {
        ProductEntity product = productDAO.findById(6);

        Assert.assertNotNull(product);
        Assert.assertNull(product.getImages());
    }
}