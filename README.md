CRUD Project - Spring + Hibernate + Embedded in-memory Database
===============================

###1. Technologies used
* Maven 3.0
* Spring 4.3.5.RELEASE
* HSQLDB 2.3.2
* Hibernate 4.3.11.Final

###2. To Run this project locally
```shell
$ git clone https://kzoid@bitbucket.org/kzoid/productcrud.git
$ mvn spring-boot:run
```
###4. Project Demo
Access ```http://localhost:8080/{serviceName}```

#### serviceName: ###
1. `/products` Get all products excluding relationships (child products, images)
2. `/products?include=all` Get all products including child product and images relationships
3. `/products?include=subproducts` Get all products including child product relationships
4. `/products?include=images` Get all products including images relationships
5. `/product/{id}` Get a product using specific product identity excluding relationships
6. `/product/{id}?include=all` Get a product using specific product identity including child product and images relationships
7. `/product/{id}?include=subproducts` Get a product using specific product identity including child product relationships
8. `/product/{id}?include=images` Get a product using specific product identity including images relationships
9. `/product/{id}/subproducts` Get set of child products for specific product
10. `/product/{id}/images` Get set of images for specific product

Pre-loaded database with Products[1-6]

###5. Infos
This project demonstrate how use in-memory database and annotations for mapping and lazy loading.
In real life projects, do not expose your domain's classes (entitys) to service's layer.